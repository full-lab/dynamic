package it.fullstack.dynamic;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import it.fullstack.dynamic.services.DynamicService;

@SpringBootTest
class MetadataServiceTest {

  @Autowired
  private DynamicService service;

  @Test
  void testGetMetadata() throws SQLException {
    var result = service.getMetadata("");
    result.forEach(System.out::println);
  }

}
