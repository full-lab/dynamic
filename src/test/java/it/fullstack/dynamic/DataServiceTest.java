package it.fullstack.dynamic;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import it.fullstack.dynamic.services.DynamicService;

@SpringBootTest
class DataServiceTest {

  @Autowired
  private DynamicService service;

  @Test
  void testGetData() {
    var result = service.findAll("");
    result.forEach(System.out::println);
  } 

}
