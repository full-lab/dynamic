package it.fullstack.dynamic.web.rest;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.dynamic.services.DynamicService;
import it.fullstack.dynamic.services.dto.MetaDataDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/tabella")
public class DynamicController {
    private final DynamicService service;

    @GetMapping("{tabella}/metadata")
    public ResponseEntity<List<MetaDataDTO>> findMetadata(@PathVariable String tabella) throws SQLException {
        log.debug("REST request to get Metadata of {}", tabella);
        
        return ResponseEntity
            .ok(service.getMetadata(tabella));
    }

    @GetMapping("{tabella}")
    public ResponseEntity<List<Map<String, Object>>> findAll(@PathVariable String tabella) {
        log.debug("REST request to get Data from: {}", tabella);
        
        return ResponseEntity
            .ok(service.findAll(tabella));
    }
    
    @GetMapping("{tabella}/{id}")
    public ResponseEntity<Map<String, Object>> findOne(@PathVariable String tabella, @PathVariable Long id) {
        log.debug("REST request to get Data from: {}, ID:{}", tabella, id);
        
        return ResponseEntity
            .ok(service.findOne(tabella, id));
    }
    
    @PutMapping("{tabella}/{id}")
    public ResponseEntity<Object> updateData(@PathVariable String tabella, @PathVariable Long id, @RequestBody Map<String, Object> data) {
        log.debug("REST request to update Data to: {}: {}", tabella, data);
        
        return ResponseEntity
            .ok(service.updateData(tabella, id, data));
    }

}
