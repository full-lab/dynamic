package it.fullstack.dynamic.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.dynamic.security.dto.UserDTO;
import it.fullstack.dynamic.services.SecurityUtils;
import it.fullstack.dynamic.services.TimatomServiceClient;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api")
public class TestRoleController {
    
  @Autowired
  TimatomServiceClient timatomServiceClient;
  
    @GetMapping("public")
    public ResponseEntity<String> publicApi() {
        log.debug("REST request to Public API");
        
        return ResponseEntity
            .ok("Public API Response");
    }

    @GetMapping("authenticated")
    public ResponseEntity<UserDTO> authenticated() {
        log.debug("REST request to get Private API");
        
        var user = SecurityUtils.getCurrentUser();
        
        return ResponseEntity
            .of(user);
    }
    
    
    @GetMapping("logout")
    public ResponseEntity<Void> logout() {
        log.debug("REST request to logout");
        
        var userToken = SecurityUtils.getCurrentUserJWT().get();
        timatomServiceClient.evictCache(userToken);
        //TODO
        // Invoca la loqout su timatom
        
        return ResponseEntity
            .ok(null);
    }
    



}
