package it.fullstack.dynamic.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import it.fullstack.dynamic.security.JWTFilter;
import it.fullstack.dynamic.services.TimatomServiceClient;
import lombok.AllArgsConstructor;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
class SecurityConfig {
  
  private final TimatomServiceClient timatomServiceClient;

  @Bean
  SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
      return http
        .csrf(AbstractHttpConfigurer::disable)
        .authorizeHttpRequests(request -> request
            .requestMatchers("/api/public/**").permitAll()
            .requestMatchers("/api/authenticated/**").authenticated()
            .requestMatchers("/api/logout").authenticated()
            .requestMatchers("/api/tabella/**").authenticated()
            .anyRequest().permitAll()
        )
        .sessionManagement(manager -> manager.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
        .addFilterBefore(jwtFilter(), UsernamePasswordAuthenticationFilter.class)
        .build();
  }
    
  private JWTFilter jwtFilter() {
    return new JWTFilter(timatomServiceClient);
  }   
}
