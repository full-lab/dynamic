package it.fullstack.dynamic.services;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.CollectionUtils;

import it.fullstack.dynamic.security.dto.UserDTO;

/**
 * Utility class for Spring Security.
 */
public class SecurityUtils {
  
  public static final String ANONYMOUS = "ROLE_ANONYMOUS";

  protected SecurityUtils() {}

  public static Optional<UserDTO> getCurrentUser() {
    return Optional
        .ofNullable(SecurityContextHolder.getContext().getAuthentication())
        .map(authentication -> (UserDTO) authentication.getPrincipal());
  }

  /**
   * Get the JWT of the current user.
   *
   * @return the JWT of the current user.
   */
  public static Optional<String> getCurrentUserJWT() {
    SecurityContext securityContext = SecurityContextHolder.getContext();
    return Optional
        .ofNullable(securityContext.getAuthentication())
        .filter(authentication -> authentication.getCredentials() instanceof String)
        .map(authentication -> (String) authentication.getCredentials());
  }

  /**
   * Check if a user is authenticated.
   *
   * @return true if the user is authenticated, false otherwise.
   */
  public static boolean isAuthenticated() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return authentication != null && getAuthorities(authentication).noneMatch(ANONYMOUS::equals);
  }

  /**
   * If the current user has a specific authority (security role).
   * <p>
   * The name of this method comes from the {@code isUserInRole()} method in the
   * Servlet API.
   *
   * @param authority the authority to check.
   * @return true if the current user has the authority, false otherwise.
   */
  public static boolean isCurrentUserInRole(String authority) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return authentication != null && getAuthorities(authentication).anyMatch(authority::equals);
  }

  /**
   * If the current user has a specific authority (security role).
   * <p>
   * The name of this method comes from the {@code isUserInRole()} method in the
   * Servlet API.
   *
   * @param authority the authority to check.
   * @return true if the current user has the authority, false otherwise.
   */
  public static boolean isCurrentUserInRole(String... authority) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    return authentication != null && CollectionUtils.containsAny(
        getAuthorities(authentication).collect(Collectors.toList()), 
        Arrays.asList(authority)
    );
  }

  private static Stream<String> getAuthorities(Authentication authentication) {
    return authentication
        .getAuthorities()
        .stream()
        .map(GrantedAuthority::getAuthority);
  }

}

