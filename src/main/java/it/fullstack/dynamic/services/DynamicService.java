package it.fullstack.dynamic.services;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.dynamic.security.dto.UserDTO;
import it.fullstack.dynamic.services.dto.MetaDataDTO;
import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class DynamicService {
  private static final String COLUMN_NAME = "COLUMN_NAME";
  private static final String TYPE_NAME = "TYPE_NAME";
  private static final String COLUMN_SIZE = "COLUMN_SIZE";
  private static final String IS_NULLABLE = "IS_NULLABLE";
  private static final String COMMENTI = "REMARKS";

  private JdbcTemplate jdbcTemplate;
  private DataSource dataSource;

  public List<MetaDataDTO> getMetadata(String tabella) throws SQLException {
    List<MetaDataDTO> columns = new ArrayList<>();

    Connection conn = dataSource.getConnection();
    DatabaseMetaData metaData = conn.getMetaData();
    ResultSet row = metaData.getColumns(null, null, tabella, null);
    while (row.next()) {
      String nomeColonna = row.getString(COLUMN_NAME);
      String tipoColonna = row.getString(TYPE_NAME);
      int dimensioneColonna = row.getInt(COLUMN_SIZE);
      boolean nullable = row.getBoolean(IS_NULLABLE);
      String commenti = row.getString(COMMENTI);

      columns.add(new MetaDataDTO(nomeColonna, tipoColonna, dimensioneColonna, nullable, commenti));
    }

    return columns;
  }

  public List<Map<String, Object>> findAll(String tabella) {
    // TODO Prendo lo User corrente e lo uso per la logica di business
    UserDTO  user = SecurityUtils.getCurrentUser().get();

    
    String sql = String.format("SELECT * FROM %s", tabella);
    return jdbcTemplate.queryForList(sql);
  }

  public Map<String, Object> findOne(String tabella, Long id) {
    String sql = String.format("SELECT * FROM %s WHERE ID = %s", tabella, id);
    return jdbcTemplate.queryForMap(sql);
  }

  public int updateData(String tabella, Long id, Map<String, Object> data) {
    String sql = String.format("UPDATE %s SET %s WHERE id = %s", tabella, getFiedPart(data), id);
    return jdbcTemplate.update(sql);
  }

  private String getFiedPart(Map<String, Object> data) {
    return data.entrySet().stream().map(entry -> entry.getKey() + " = '" + entry.getValue() + "'")
        .collect(Collectors.joining(", "));
  }

}
