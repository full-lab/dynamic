package it.fullstack.dynamic.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import it.fullstack.dynamic.security.dto.UserDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class TimatomServiceClient {

  @Value("${application.services.iam}")
  private String url;
  private final RestTemplate restTemplate;
  
  // FIXME Non funziona
  @CacheEvict(value = "profileCache")
  public void evictCache(String jwt) {
    log.info("REST request to evict cache:{}", jwt);
  }

  @Cacheable(value = "profileCache")
  public UserDTO getUserProfile(String jwt) {
    log.info("Getting user profile from Timatom for Token: {}", jwt);
    
    // Crea gli headers
    HttpHeaders headers = new HttpHeaders();
    headers.set("token", jwt);

    // Crea l'entità da passare, con i headers e senza body
    HttpEntity<String> headerEntity = new HttpEntity<>(headers);

    // Effettua la richiesta con gli headers
    ResponseEntity<UserDTO> response = restTemplate.exchange(url + "/apison/v1/user/profile", HttpMethod.POST, headerEntity, UserDTO.class);

    return response.getBody();
  }
  
  public void logoutProfile(String jwt) {
    log.info("Logout user from Timatom for Token: {}", jwt);
    
    // TODO Da implmentare
  }
  
  public UserDTO verifyToken(String jwtToken) {
    log.info("Verifying Token from Timatom: {}", jwtToken);
    
    // Crea gli headers
    HttpHeaders headers = new HttpHeaders();
    headers.set("token", jwtToken);

    // Crea l'entità da passare, con i headers e senza body
    HttpEntity<String> entity = new HttpEntity<>(headers);

    // Effettua la richiesta con gli headers
    ResponseEntity<UserDTO> response = restTemplate.exchange(url + "/apison/v1/user/token", HttpMethod.POST, entity, UserDTO.class);

    return response.getBody();
  }
}
