package it.fullstack.dynamic.services.dto;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class UpdateDataDTO {
  Map<String, Object> data;
}
