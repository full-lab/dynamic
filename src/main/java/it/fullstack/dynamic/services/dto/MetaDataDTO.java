package it.fullstack.dynamic.services.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MetaDataDTO {
  private String nome;
  private String tipo;
  private int dimensione;
  private boolean nullable;
  private String commenti;
}
