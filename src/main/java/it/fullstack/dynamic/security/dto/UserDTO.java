package it.fullstack.dynamic.security.dto;

import java.util.List;

import lombok.Data;

@Data
public class UserDTO {
  private String status;
  private String msg;
  private User whoami;
  private List<Feature> features;
  private List<Group> groups;
  private List<Role> roles;
  private List<Region> regions;
  private List<Vendor> vendors;
  private List<App> apps;
}
