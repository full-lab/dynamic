package it.fullstack.dynamic.security.dto;

import lombok.Data;

@Data
public class Vendor {
  private long id;
  private String name;
}
