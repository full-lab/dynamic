package it.fullstack.dynamic.security.dto;

import lombok.Data;

@Data
public class User {
  private long id;
  private String username;
  private String email;
  private String department;
}
