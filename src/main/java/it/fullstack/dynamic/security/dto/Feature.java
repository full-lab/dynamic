package it.fullstack.dynamic.security.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Feature {
  private int id;
  private String name;
  private String description;

  @JsonProperty("can_read")
  private boolean canRead;

  @JsonProperty("can_insert")
  private boolean canInsert;

  @JsonProperty("can_modify")
  private boolean canModify;

  @JsonProperty("can_delete")
  private boolean canDelete;

  @JsonProperty("can_extra")
  private boolean canExtra;

  public void setCanRead(String canRead) {
    this.canRead = "Yes".equalsIgnoreCase(canRead);
  }

  public void setCanInsert(String canInsert) {
    this.canInsert = "Yes".equalsIgnoreCase(canInsert);
  }

  public void setCanModify(String canModify) {
    this.canModify = "Yes".equalsIgnoreCase(canModify);
  }

  public void setCanDelete(String canDelete) {
    this.canDelete = "Yes".equalsIgnoreCase(canDelete);
  }

  public void setCanExtra(String canExtra) {
    this.canExtra = "Yes".equalsIgnoreCase(canExtra);
  }
}
