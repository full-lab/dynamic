package it.fullstack.dynamic.security.dto;

import lombok.Data;

@Data
public class Group {
  private long id;
  private String name;
  private String description;
}