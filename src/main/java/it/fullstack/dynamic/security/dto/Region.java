package it.fullstack.dynamic.security.dto;

import lombok.Data;

@Data
public class Region {
  private long id;
  private String name;
}
