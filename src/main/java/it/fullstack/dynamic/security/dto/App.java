package it.fullstack.dynamic.security.dto;

import lombok.Data;

@Data
public class App {
  private long id;
  private String name;
}
