package it.fullstack.dynamic.security;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import it.fullstack.dynamic.security.dto.UserDTO;
import it.fullstack.dynamic.services.TimatomServiceClient;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Filters incoming requests and installs a Spring Security principal if a header corresponding to a valid user is found.
 */
@Slf4j
@AllArgsConstructor
public class JWTFilter extends GenericFilterBean {
  public static final String AUTHORIZATION_HEADER = "token";
  private final TimatomServiceClient timatomServiceClient;

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain filter) throws IOException, ServletException {
    HttpServletRequest httpServletRequest = (HttpServletRequest) req;
    String jwt = resolveToken(httpServletRequest);
    
    log.info("\n\r");
    log.info("JWTFilter Token: {}", jwt);

    if (StringUtils.hasText(jwt)) {
      try {
        Authentication authentication = createAuthentication(jwt);
        SecurityContextHolder.getContext().setAuthentication(authentication);
      } catch (Exception e) {
        log.warn("Error creating Profile for Token:{}, {}", jwt, e.getMessage());
      }
    }
    filter.doFilter(req, res);
  }

  public Authentication createAuthentication(String jwt) {
    UserDTO profile = timatomServiceClient.getUserProfile(jwt);

    /*
     * Mappare i ruoli in GrantedAuthority
     */
    //      Set<GrantedAuthority> authorities = Arrays
    //          .stream(claims.get(AUTHORITIES_KEY).toString().split(","))
    //          .filter(r -> !r.isEmpty())
    //          .map(SimpleGrantedAuthority::new)
    //          .collect(Collectors.toSet());
    Set<GrantedAuthority> authorities = new HashSet<>();

    log.info("Creating Authentication for User: {}", profile.getWhoami().getUsername());
    return new UsernamePasswordAuthenticationToken(profile, jwt, authorities);
  }

  public static String resolveToken(HttpServletRequest request) {
    log.debug("Searching JWT Token on Request Parameter...");
    String bearerToken = request.getParameter(AUTHORIZATION_HEADER);
    log.debug("JWT Token on Request Parameter:{}", bearerToken);

    if (bearerToken == null) {
      log.debug("Searching JWT Token on Header...");
      bearerToken = request.getHeader(AUTHORIZATION_HEADER);
      log.debug("JWT Token on Header:{}", bearerToken);
    }

    if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
      return bearerToken.substring(7);
    }
    return bearerToken;
  }
}
