# Dynamic

## Db Sample
- sql/dump.sql

## Sample API
http://localhost:8080/api/tabella/tabella_demo/metadata (Metadata)
http://localhost:8080/api/tabella/tabella_demo (Dati)

http://localhost:8080/api/tabella/tcoverage/metadata (Metadata)
http://localhost:8080/api/tabella/tcoverage (Dati)


## Security API
http://localhost:8080/api/public (API Pubblica non necessita di Token)

http://localhost:8080/api/authenticated?token=[token] (API Protetta necessita di Token)
- Invocare più volte con lo stesso token e notare che solo ogni minuto [parametro configurabile] avviene la chiamata del profile verso lo IAM
- Provare a cambiare token e notare nei log la prima invocazione verso lo IAM
