-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: localhost    Database: dynamic
-- ------------------------------------------------------
-- Server version	8.0.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `stato`
--

DROP TABLE IF EXISTS `stato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stato` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(63) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `id_parent` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`),
  CONSTRAINT `stato_ibfk_1` FOREIGN KEY (`id_parent`) REFERENCES `stato` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stato`
--

LOCK TABLES `stato` WRITE;
/*!40000 ALTER TABLE `stato` DISABLE KEYS */;
INSERT INTO `stato` VALUES (1,'Coverage','Stati su ms Coverage (associato a tabella tcoverage)',NULL),(2,'Draft','Stato in cui l\'oggetto è visibile solo all\'autore (id_user_created)',1),(3,'Inserito','Stato in cui l\'oggetto è visibile anche a chi ne ha diritto',1),(4,'Validato','Stato in cui l\'oggetto è visibile a chi ne ha diritto',1),(5,'Chiuso','Stato in cui l\'oggetto è visibile a chi ne ha diritto',1);
/*!40000 ALTER TABLE `stato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabella_2`
--

DROP TABLE IF EXISTS `tabella_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tabella_2` (
  `id` int NOT NULL,
  `name` varchar(31) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` varchar(1023) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date_creation` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `id_user_created` int DEFAULT NULL,
  `id_user_modified` int DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `latitude` varchar(31) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `longitude` varchar(31) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `priority` enum('High','Medium','Low') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'Low',
  `operative_note` varchar(511) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `expected_date` date DEFAULT NULL,
  `id_status_process` int DEFAULT NULL,
  `id_parent` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user_created` (`id_user_created`),
  KEY `id_user_modified` (`id_user_modified`),
  KEY `id_status_process` (`id_status_process`),
  KEY `id_parent` (`id_parent`),
  CONSTRAINT `tabella_2_ibfk_1` FOREIGN KEY (`id_user_created`) REFERENCES `utente` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tabella_2_ibfk_2` FOREIGN KEY (`id_user_modified`) REFERENCES `utente` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tabella_2_ibfk_3` FOREIGN KEY (`id_parent`) REFERENCES `tabella_2` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tabella_2_ibfk_4` FOREIGN KEY (`id_status_process`) REFERENCES `stato` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin COMMENT='table of coverage';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabella_2`
--

LOCK TABLES `tabella_2` WRITE;
/*!40000 ALTER TABLE `tabella_2` DISABLE KEYS */;
INSERT INTO `tabella_2` VALUES (1,'Sito1','descrizione Sito1','2024-02-15 09:38:32',NULL,1,NULL,'Via Da Qui, 28\r\nTorino','45.07','7.67','Low',NULL,'2024-03-30',NULL,NULL),(2,'Sito2','descrizione Sito2','2024-02-15 09:38:32',NULL,2,NULL,'Via Da Qui, 120\r\nTorino','45.08','7.7','Low',NULL,'2024-03-30',3,NULL);
/*!40000 ALTER TABLE `tabella_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabella_demo`
--

DROP TABLE IF EXISTS `tabella_demo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tabella_demo` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `PrimaColonna` varchar(100) DEFAULT NULL,
  `SecondaColonna` datetime DEFAULT NULL,
  `TerzaColonna` int DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabella_demo`
--

LOCK TABLES `tabella_demo` WRITE;
/*!40000 ALTER TABLE `tabella_demo` DISABLE KEYS */;
INSERT INTO `tabella_demo` VALUES (1,'Valore 1','2024-01-01 00:00:00',123),(2,'Valore 2','2020-01-01 00:00:00',12);
/*!40000 ALTER TABLE `tabella_demo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente`
--

DROP TABLE IF EXISTS `utente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `utente` (
  `id` int NOT NULL,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente`
--

LOCK TABLES `utente` WRITE;
/*!40000 ALTER TABLE `utente` DISABLE KEYS */;
INSERT INTO `utente` VALUES (1,'utente1','inseritore'),(2,'utente2','validatore');
/*!40000 ALTER TABLE `utente` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-14 12:39:19
